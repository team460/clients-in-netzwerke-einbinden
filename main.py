import sys

# ip address input
ip_address = input("Please specify the IP address to be split: ")

# ip address verification
if 7 <= len(ip_address) < 16:
    print("The specified IP address '" + ip_address + "' is used in the following calculation!")
else:
    print("You must specify an IPv4 address in dotted decimal notation!")
    print(sys.exit())

# splitting the octets ip address
ip_address_split = ip_address.split('.')
print(ip_address_split)

# converting the split ip address  to integer
ip_address_split_map = map(int, ip_address_split)
ip_address_split_list = list(ip_address_split_map)
print(ip_address_split_list)

# converting ip address from integer into binary
first_octet_ip_address = format(ip_address_split_list[-4], "b")
print(first_octet_ip_address)
second_octet_ip_address = format(ip_address_split_list[-3], "b")
print(second_octet_ip_address)
third_octet_ip_address = format(ip_address_split_list[-2], "b")
print(third_octet_ip_address)
fourth_octet_ip_address = format(ip_address_split_list[-1], "b")
print(fourth_octet_ip_address)

# subnet mask input
subnet_mask = (input("Please specify the subnet mask: "))

# subnet mask verification
if 7 <= len(subnet_mask) < 16:
    print("The specified subnet mask '" + subnet_mask + "' is used in the following calculation!")
else:
    print("You must specify a subnet mask in dotted decimal notation!")
    print(sys.exit())

# splitting the octets subnet mask
subnet_mask_split = subnet_mask.split('.')
print(subnet_mask_split)

# converting the split subnet mask to integer
subnet_mask_split_map = map(int, subnet_mask_split)
subnet_mask_split_list = list(subnet_mask_split_map)
print(subnet_mask_split_list)

# converting subnet mask from integer into binary
first_octet_subnet_mask = format(subnet_mask_split_list[-4], "b")
print(first_octet_subnet_mask)
second_octet_subnet_mask = format(subnet_mask_split_list[-3], "b")
print(second_octet_subnet_mask)
third_octet_subnet_mask = format(subnet_mask_split_list[-2], "b")
print(third_octet_subnet_mask)
fourth_octet_subnet_mask = format(subnet_mask_split_list[-1], "b")
print(fourth_octet_subnet_mask)

# calculating cidr from specified subnet mask
cidr = first_octet_subnet_mask.count('1') \
         + second_octet_subnet_mask.count('1') \
         + third_octet_subnet_mask.count('1') \
         + fourth_octet_subnet_mask.count('1')

# converting cidr from int to string for print
cidr_as_string = str(cidr)
print("The CIDR from the specified subnet mask is '/" + cidr_as_string + "'")

# calculation of usable ip addresses and total ip addresses from the specified network
if 30 >= cidr >= 23:
    host_bits = (32-cidr)
    total_ip_addresses = (2 ** host_bits)
    usable_ip_addresses = (2 ** host_bits - 2)
elif cidr == 31:
    host_bits = 1
    total_ip_addresses = 2
    usable_ip_addresses = 2
    print("Please note, that your specified network has no Broadcast address. The network is a point to point link.")
elif cidr == 32:
    host_bits = 0
    total_ip_addresses = 1
    usable_ip_addresses = 1
    print("Please note, that the network has only a single IPv4 address and all traffic will go directly between the "
          "device with that IPv4 address and the default gateway. The device would not be able to communicate with "
          "other devices on the network.")
else:
    host_bits = ''
    total_ip_addresses = ''
    usable_ip_addresses = ''
    print("Error. Program closes")

# converting usable and total ip addresses from int to string for print
total_ip_addresses_as_string = str(total_ip_addresses)
usable_ip_addresses_as_string = str(usable_ip_addresses)

# print host amounts from specified network
print("Total IP Addresses: " + total_ip_addresses_as_string)
print("Usable IP Addresses: " + usable_ip_addresses_as_string)

# converting fourth octet from specified ip address into a list
fourth_octet_string = str(fourth_octet_ip_address)
fourth_octet_map = map(int, fourth_octet_string)
fourth_octet_list = list(fourth_octet_map)
print(fourth_octet_list)

# calculating first IP address
if host_bits == 7:

    scan_digit1 = fourth_octet_list[-8]
    if scan_digit1 == 1:
        scan_digit1 = 0
    else:
        scan_digit2 = 1
    scan_digit2 = fourth_octet_list[-7]
    if scan_digit2 == 1:
        scan_digit2 = 0
    else:
        scan_digit2 = 1
    scan_digit3 = fourth_octet_list[-6]
    if scan_digit3 == 1:
        scan_digit3 = 0
    else:
        scan_digit3 = 1
    scan_digit4 = fourth_octet_list[-5]
    if scan_digit4 == 1:
        scan_digit4 = 0
    else:
        scan_digit4 = 1
    scan_digit5 = fourth_octet_list[-4]
    if scan_digit5 == 1:
        scan_digit5 = 0
    else:
        scan_digit5 = 1
    scan_digit6 = fourth_octet_list[-3]
    if scan_digit6 == 1:
        scan_digit6 = 0
    else:
        scan_digit6 = 1
    scan_digit7 = fourth_octet_list[-2]
    if scan_digit7 == 1:
        scan_digit7 = 0
    else:
        scan_digit7 = 1
    scan_digit8 = fourth_octet_list[-1]
    if scan_digit8 == 1:
        scan_digit8 = 0
    else:
        scan_digit8 = 1

    # converting into fourth octet ip address
    fourth_octet_reversed = [scan_digit1, scan_digit2, scan_digit3, scan_digit4, scan_digit5, scan_digit6, scan_digit7,
                             scan_digit8]
    b = ''.join(map(str, fourth_octet_reversed))
    a = (int(b, 2)) + 2

    first_ip_range = [ip_address_split_list[-2], ip_address_split_list[-3], ip_address_split_list[-4], a]
    c = ''.join(map(str, first_ip_range))
    print(c)


elif host_bits == 6:
    scan_digit2 = fourth_octet_list[-7]
    scan_digit3 = fourth_octet_list[-6]
    scan_digit4 = fourth_octet_list[-5]
    scan_digit5 = fourth_octet_list[-4]
    scan_digit6 = fourth_octet_list[-3]
    scan_digit7 = fourth_octet_list[-2]
    scan_digit8 = fourth_octet_list[-1]

elif host_bits == 5:
    scan_digit3 = fourth_octet_list[-6]
    scan_digit4 = fourth_octet_list[-5]
    scan_digit5 = fourth_octet_list[-4]
    scan_digit6 = fourth_octet_list[-3]
    scan_digit7 = fourth_octet_list[-2]
    scan_digit8 = fourth_octet_list[-1]

elif host_bits == 4:
    scan_digit4 = fourth_octet_list[-5]
    scan_digit5 = fourth_octet_list[-4]
    scan_digit6 = fourth_octet_list[-3]
    scan_digit7 = fourth_octet_list[-2]
    scan_digit8 = fourth_octet_list[-1]

elif host_bits == 3:
    scan_digit5 = fourth_octet_list[-4]
    scan_digit6 = fourth_octet_list[-3]
    scan_digit7 = fourth_octet_list[-2]
    scan_digit8 = fourth_octet_list[-1]


elif host_bits == 2:
    scan_digit6 = fourth_octet_list[-3]
    scan_digit7 = fourth_octet_list[-2]
    scan_digit8 = fourth_octet_list[-1]

elif host_bits == 1:
    scan_digit7 = fourth_octet_list[-2]
    scan_digit8 = fourth_octet_list[-1]

elif host_bits == 0:
    scan_digit7 = fourth_octet_list[-1]
    scan_digit8 = fourth_octet_list[0]

# exit program
print("Program closes")
print(sys.exit())
