import sys

# ip address input
ip_address = input("Please specify the IP address: ")
# print(ip_address)

# ip address verification
if 7 <= len(ip_address) < 16:
    print("The specified IP address '" + ip_address + "' is used in the following calculation!")
else:
    print("You must specify an IPv4 address in dotted decimal notation!")
    print(sys.exit())

# ip address split
ip_address_split = ip_address.split('.')
# print(ip_address_split)

# converting the split ip address  to integer
ip_address_split_map = map(int, ip_address_split)
ip_address_split_list = list(ip_address_split_map)
# print(ip_address_split_list)

# subnet mask input
subnet_mask = input("Please specify the subnet mask: ")
# print(subnet_mask)

# subnet mask verification
if 7 <= len(subnet_mask) < 16:
    print("The specified subnet mask '" + subnet_mask + "' is used in the following calculation!")
else:
    print("You must specify a subnet mask in dotted decimal notation!")
    print(sys.exit())

# subnet mask split
subnet_mask_split = subnet_mask.split('.')
# print(subnet_mask_split)

# converting the split subnet mask to integer
subnet_mask_split_map = map(int, subnet_mask_split)
subnet_mask_split_list = list(subnet_mask_split_map)
# print(subnet_mask_split_list)

# calculate network address
first_octet = (ip_address_split_list[-4] & subnet_mask_split_list[-4])
second_octet = (ip_address_split_list[-3] & subnet_mask_split_list[-3])
third_octet = (ip_address_split_list[-2] & subnet_mask_split_list[-2])
fourth_octet = (ip_address_split_list[-1] & subnet_mask_split_list[-1])
network_address = str(first_octet) + "." + str(second_octet) + "." + str(third_octet) + "." + str(fourth_octet)
print("Network Address:", network_address)

# calculating cidr from specified subnet mask
first_octet_subnet_mask = format(subnet_mask_split_list[-4], "b")
# print(first_octet_subnet_mask)
second_octet_subnet_mask = format(subnet_mask_split_list[-3], "b")
# print(second_octet_subnet_mask)
third_octet_subnet_mask = format(subnet_mask_split_list[-2], "b")
# print(third_octet_subnet_mask)
fourth_octet_subnet_mask = format(subnet_mask_split_list[-1], "b")
# print(fourth_octet_subnet_mask)

cidr = first_octet_subnet_mask.count('1') \
         + second_octet_subnet_mask.count('1') \
         + third_octet_subnet_mask.count('1') \
         + fourth_octet_subnet_mask.count('1')

cidr_as_string = str(cidr)
print("CIDR:", cidr_as_string)

# calculation of usable ip addresses and total ip addresses from the specified network
if 30 >= cidr >= 1:
    host_bits = (32-cidr)
    total_ip_addresses = (2 ** host_bits)
    usable_ip_addresses = (2 ** host_bits - 2)
elif cidr == 31:
    host_bits = 1
    total_ip_addresses = 2
    usable_ip_addresses = 2
    print("Please note, that your specified network has no Broadcast address. The network is a point to point link.")
elif cidr == 32:
    host_bits = 0
    total_ip_addresses = 1
    usable_ip_addresses = 1
    print("Please note, that the network has only a single IPv4 address and all traffic will go directly between the "
          "device with that IPv4 address and the default gateway. The device would not be able to communicate with "
          "other devices on the network.")
else:
    host_bits = ''
    total_ip_addresses = ''
    usable_ip_addresses = ''
    print("Error. Program closes")
    print(sys.exit())

# converting usable and total ip addresses from int to string for print
total_ip_addresses_as_string = str(total_ip_addresses)
usable_ip_addresses_as_string = str(usable_ip_addresses)

# print host amounts from specified network
print("Total IP Addresses: " + total_ip_addresses_as_string)
print("Usable IP Addresses: " + usable_ip_addresses_as_string)

# fill in missing zeros in subnet mask octets
subnet_mask_split_list_4_as_string = str(format(subnet_mask_split_list[-1], "b"))
filled_subnet_mask_split_list_4 = subnet_mask_split_list_4_as_string.ljust(8, '0')
# print(filled_subnet_mask_split_list_4)

subnet_mask_split_list_3_as_string = str(format(subnet_mask_split_list[-2], "b"))
filled_subnet_mask_split_list_3 = subnet_mask_split_list_3_as_string.ljust(8, '0')
# print(filled_subnet_mask_split_list_3)

subnet_mask_split_list_2_as_string = str(format(subnet_mask_split_list[-3], "b"))
filled_subnet_mask_split_list_2 = subnet_mask_split_list_2_as_string.ljust(8, '0')
# print(filled_subnet_mask_split_list_2)

subnet_mask_split_list_1_as_string = str(format(subnet_mask_split_list[-4], "b"))
filled_subnet_mask_split_list_1 = subnet_mask_split_list_1_as_string.ljust(8, '0')
# print(filled_subnet_mask_split_list_1)

# inverting subnet mask
inverted_subnet_mask_split_list_1 = ''
for i in filled_subnet_mask_split_list_1:
    if i == '0':
        inverted_subnet_mask_split_list_1 += '1'
    else:
        inverted_subnet_mask_split_list_1 += '0'
# print(inverted_subnet_mask_split_list_1)

inverted_subnet_mask_split_list_2 = ''
for i in filled_subnet_mask_split_list_2:
    if i == '0':
        inverted_subnet_mask_split_list_2 += '1'
    else:
        inverted_subnet_mask_split_list_2 += '0'
# print(inverted_subnet_mask_split_list_2)

inverted_subnet_mask_split_list_3 = ''
for i in filled_subnet_mask_split_list_3:
    if i == '0':
        inverted_subnet_mask_split_list_3 += '1'
    else:
        inverted_subnet_mask_split_list_3 += '0'
# print(inverted_subnet_mask_split_list_3)

inverted_subnet_mask_split_list_4 = ''
for i in filled_subnet_mask_split_list_4:
    if i == '0':
        inverted_subnet_mask_split_list_4 += '1'
    else:
        inverted_subnet_mask_split_list_4 += '0'
# print(inverted_subnet_mask_split_list_4)

# calculate broadcast address
broadcast_first_octet = ip_address_split_list[-4] | int(inverted_subnet_mask_split_list_1, 2)

broadcast_second_octet = ip_address_split_list[-3] | int(inverted_subnet_mask_split_list_2, 2)

broadcast_third_octet = ip_address_split_list[-2] | int(inverted_subnet_mask_split_list_3, 2)

broadcast_fourth_octet = ip_address_split_list[-1] | int(inverted_subnet_mask_split_list_4, 2)

broadcast_address = str(broadcast_first_octet) + "." + str(broadcast_second_octet) + "." \
                    + str(broadcast_third_octet) + "." + str(broadcast_fourth_octet)
if 30 >= cidr >= 1:
    print("Broadcast Address:", broadcast_address)
elif cidr == 31:
    print("This network has no broadcast address")
elif cidr == 32:
    print("This network has no broadcast address")
else:
    print("Error. Program closes")
    print(sys.exit())

# calculating first host ip address
first_host_ip_fourth_octet = fourth_octet + 1
# print(first_host_ip_fourth_octet)

first_host_ip_address = str(first_octet) + "." + str(second_octet) + "." + str(third_octet) + "." \
                        + str(first_host_ip_fourth_octet)
if 31 >= cidr >= 1:
    print("First Host IP Address:", first_host_ip_address)
elif cidr == 32:
    print("First Host IP Address: This network has only the IP Address '" + network_address + "'!")
else:
    print("Error. Program closes")
    print(sys.exit())

# calculating last host ip address
last_host_ip_fourth_octet = broadcast_fourth_octet - 1
# print(last_host_ip_fourth_octet)

last_host_ip_address = str(broadcast_first_octet) + "." + str(broadcast_second_octet) + "." \
                    + str(broadcast_third_octet) + "." + str(last_host_ip_fourth_octet)
if 31 >= cidr >= 1:
    print("Last Host IP Address:", last_host_ip_address)
elif cidr == 32:
    print("Last Host IP Address: There is no second IP Address!")
else:
    print("Error. Program closes")
    print(sys.exit())
